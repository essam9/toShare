import * as firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCGTFEbmZAg9gnDYSIKbRubz9ymJKTxDuM",
  authDomain: "next-283218.firebaseapp.com",
  databaseURL: "https://next-283218.firebaseio.com",
  projectId: "next-283218",
  storageBucket: "next-283218.appspot.com",
  messagingSenderId: "363937834767",
  appId: "1:363937834767:web:526a25b5a5de2b8069c07a",
  measurementId: "G-RKG4EBC5E4",
};
export default function initialize() {
  try {
    //let firebase = {};
    firebase.initializeApp(firebaseConfig);
    console.log("initialize");
  } catch (err) {
    console.log(err);
  }
  /*
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // User is signed in.
      console.log(user);
    } else {
      // No user is signed in.
      console.log("firebaseConfig no user");
    }
  });
  */
}
