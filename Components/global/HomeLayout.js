import Head from "next/head";
import Link from "next/link";
import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import HomeIcon from "@material-ui/icons/Home";
import SearchIcon from "@material-ui/icons/Search";
import NotificationsIcon from "@material-ui/icons/Notifications";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import Drawer from "./Drawer";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import CssBaseline from "@material-ui/core/CssBaseline";
import styles from "./HomeLayout.module.scss";

import React, { useState, useEffect } from 'react';

import initialize from "../../firebase-config";
import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

initialize();



const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    position: "relative",
    flexDirection: "column",
    minHeight: "100vh",
    padding: "0px",
  },
  main: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(2),
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(8),
    right: theme.spacing(2),
  },
  footer: {
    marginTop: "auto",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[800],
  },
}));

export default function HomeLayout() {
  const classes = useStyles();

  const [value, setValue] = React.useState("recents");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleClick = (event, newValue) => {
    setValue(newValue);
    //refresh feed
    console.log('refreshing feed');

  };



















    const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  var data = [];
//const classes = useStyles();
useEffect(() => {
    // Update the document title using the browser API
 //document.title = `You clicked times`;
  firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = user.photoURL;
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    var providerData = user.providerData;
    console.log(user.uid);
    //window.location = "http://localhost:3000/home2";
    //window.location = "home2";
    firebase.firestore().collection("activities").limit(20)//where("capital", "==", true)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
          /*  var x = document.createElement("P");                        // Create a <p> node
            var t = document.createTextNode(doc.data().content);    // Create a text node
          x.appendChild(t);                                        // Append the text to <p>
          var y = document.createElement("div");
          y.innerHTML = "<p>fghhgf</p>";
            document.getElementById('container').appendChild(x);
          document.getElementById('container').appendChild(y);
*/
          setIsLoaded(true);
          data[doc.id]=doc.data();
          setItems(data);
        

        });

      console.log(items);
//console.log(data);
      
    })
      .catch(function (error) {
      setIsLoaded(true);
          setError(error);
        console.log("Error getting documents: ", error);
      });
  } else {
    // User is signed out.
    // ...
    window.location = "/signin";
  }
});
  },[]);



  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" className={classes.root}>
        <Drawer />
        <div id="container">
          {items.map(item => (
          <li key={item.name}>
            {item.content} {item.uid}
          </li>
        ))}
        </div>
        <Link href="/events/create">
          <Fab color="primary" aria-label="add" className={classes.fab}>
            <AddIcon />
          </Fab>
        </Link>

        <BottomNavigation
          value={value}
          onChange={handleChange}
          className={classes.footer}
        >
          <BottomNavigationAction
            label="Home"
            value="home"
            onClick={handleClick}
            icon={<HomeIcon />}
          />
          <Link href="/search">
            <BottomNavigationAction
            label="Search"
            value="search"
            icon={<SearchIcon />}
          />
          </Link><Link href="/notifications">
          <BottomNavigationAction
            label="Notifications"
            value="notifications"
            icon={<NotificationsIcon />}
          /></Link><Link href="/messages">
          <BottomNavigationAction
            label="Messages"
            value="messages"
            icon={<MailOutlineIcon />}
          /></Link>
        </BottomNavigation>
      </Container>
    </React.Fragment>
  );
}
