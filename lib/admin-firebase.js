var admin = require("firebase-admin");
import cookies from 'next-cookies';
var db;

export async function initialize(){
const serviceAccount = await require("../next-283218-firebase-adminsdk-t5k9v-efe01ca1e9.json");
try {
admin.initializeApp({credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://next-283218.firebaseio.com"});
    db = admin.firestore();
    return true;
} catch (err) {
    if (!/already exists/.test(err.message)) {console.error('Firebase initialization error', err.stack);}
    return false;
}
}

export function isAuthenticated(context){
    const idToken = cookies(context).myCookie || '';
    admin.auth().verifyIdToken(idToken)
        .then(function(decodedToken) {
            let uid = decodedToken.uid;
            console.log('uid: '+uid);
            return uid;
        }).catch(function(error) {
            if (!context && !context.res){console.log('a7a: '+uid);}
            if (context.res) {
                try{
                    return context.res.writeHead(302, { Location: '/signup' });
                    context.res.end();
                    context.res.finished = true
                }catch(error){}
            }
            return {props: {},}; // will be passed to the page component as props};
        }); 
    return {
        props: {}, // will be passed to the page component as props
    }
}

export async function chats(uid){
    const db = admin.firestore();
    var chatsRef = db.collection('chats');
    const snapshot = await chatsRef.orderBy('timestamp').where('members', 'array-contains',uid).get();
    if (snapshot.empty) {
        console.log('No matching documents.');
        return {'empty':true};
    }  
    snapshot.forEach(doc => {console.log(doc.id, '=>', doc.data());});
    return snapshot;
}

export async function messages(chatRef){//uid
    var messagesRef = db.ref("/messages/"+chatRef);
    const snapshot = await messagesRef.orderBy('timestamp').get();
    if (snapshot.empty) {
        console.log('No matching documents.');
        return;
    }  
    snapshot.forEach(doc => {console.log(doc.id, '=>', doc.data());});
    return snapshot;
}

export function updateChats(chatId,data){
    var chatRef = db.collection("messages").doc(chatId);
    //data={lastMessage: true,status:true,timestamp:true,};
    return chatRef.update(data)
      .then(function() {
        console.log("Document successfully updated!");
      })
      .catch(function(error) {
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
      });
}

export function createChat(data){
    return chatRef = db.collection("chats").doc();
}

export function createMessage(chatId,data){
    var lastMessageRef = db.collection("messages"+chatId).doc();
data={
  from: from,
  to: to,
  meassage: message,
  status:status,
  timestamp: firebase.firestore.FieldValue.serverTimestamp()
  };
// later...
lastMessageRef.set(data);
}