import styled, { css } from "styled-components";

const Button = styled.button`

root: {
    width: "100%",
    minHeight: "80vh",
    maxWidth: "36ch",
    backgroundColor: theme.palette.background.paper,
  },

  inline: {
    display: "inline",
  },
  root2: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  textfield: {
    flexGrow: 1,
    position: "absolute",
    bottom: "10px",
    width: "80%",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(0),
  },
}
`;
