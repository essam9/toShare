import styled, { css } from "styled-components";
const SignInButton = styled.button`
  border-radius: 1.2rem;
  margin: 0.5rem;
`;
