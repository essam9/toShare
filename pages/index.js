import Head from "next/head";
import Link from "next/link";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import styles from "./index.module.scss";

import { useRouter } from "next/router";
import initialize from "./../firebase-config";
import * as firebase from "firebase/app";

import "firebase/auth";

initialize();
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = user.photoURL;
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    var providerData = user.providerData;
    console.log(user.uid);
    window.location = "http://localhost:3000/home";
    window.location = "home";
    const router = useRouter();
    router.push("/home");
  } else {
    // User is signed out.
    // ...
  }
});

export default function ActiveLink({ children, href }) {
  //
  return (
    <div className="container">
      <Head>
        <title>toShare *</title>
        <link rel="icon" href="/images/popcorn2.png" />
      </Head>

      <main>
        <Avatar src="/images/popcorn2.png"></Avatar>
        <h3 className="title">
          <Link href="/">
            <a>toShare *</a>
          </Link>
        </h3>

        <p className="description">real Sharing of Everyday's Activities</p>
        <div>
          <Button
            variant="contained"
            color="primary"
            className="signInButton"
          >
            <Link href="/signin">
              <a>Log In</a>
            </Link>
          </Button>
          <Button
            variant="contained"
            color="secondary"
            className="signInButton"
          >
            <Link href="/signup">
              <a>Sign Up</a>
            </Link>
          </Button>
        </div>
      </main>
      <footer>
        <Link href="/">
          <a>
            copyright 2020{" "}
            <img
              src="/images/popcorn2.png"
              alt="toShare Logo"
              className="logo"
            />
          </a>
        </Link>
      </footer>

      <style jsx>{`
        button {
          margin: 0.5rem;
        }
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 3rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        footer {
          width: 100%;
          height: 100px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        footer img {
          margin-left: 0.5rem;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .title a {
          color: #0070f3;
          text-decoration: none;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: none;
        }

        .title {
          margin: 0;
          margin-top: 2rem;
          line-height: 1.15;
          font-size: 4rem;
        }

        .title,
        .description {
          text-align: center;
        }

        .description {
          line-height: 1.5;
          font-size: 1.5rem;
        }

        code {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
          font-size: 1.1rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
            DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          max-width: 800px;
          margin-top: 3rem;
        }

        .card {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }

        .card:hover,
        .card:focus,
        .card:active {
          color: #0070f3;
          border-color: #0070f3;
        }

        .card h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }

        .card p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }

        .logo {
          height: 1em;
        }
.signInButton{margin: 0.5rem !important;border-radius:1.2rem !important;}
        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
        .signInButton{margin: 0.5rem !important;border-radius:1.2rem !important;}
      `}</style>
    </div>
  );
}
