import Head from "next/head";
import Link from "next/link";
import HomeLayout from "../Components/global/HomeLayout";
//import * as admin from 'firebase-admin';
var admin = require("firebase-admin");
import cookies from "next-cookies";
//import firebase from 'firebase';

// Configure Firebase.
const config = {
  apiKey: "AIzaSyCGTFEbmZAg9gnDYSIKbRubz9ymJKTxDuM",
  authDomain: "localhost",
  // ...https://console.developers.google.com/apis/credentials/key/01d0ebb9-a034-4a99-9198-bed2c0037c58?folder=&organizationId=&project=next-283218
};
export async function getServerSideProps(context) {
  //https://console.firebase.google.com/project/next-283218/settings/serviceaccounts/adminsdk
  const serviceAccount = await require("../next-283218-firebase-adminsdk-t5k9v-efe01ca1e9.json");
  //console.log(serviceAccount);
  try {
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: "https://next-283218.firebaseio.com",
    });
  } catch (err) {
    // we skip the "already exists" message which is
    // not an actual error when we're hot-reloading
    if (!/already exists/.test(err.message)) {
      console.error("Firebase initialization error", err.stack);
      return {
        props: {}, // will be passed to the page component as props
      };
    }
  }
  const idToken = cookies(context).myCookie || "";
  //console.log('idToken: '+idToken);
  admin
    .auth()
    .verifyIdToken(idToken)
    .then(function (decodedToken) {
      let uid = decodedToken.uid;
      // ...
      console.log("uid: " + uid);
    })
    .catch(function (error) {
      // Handle error

      if (!context && !context.res) {
        console.log("a7a: " + uid);
      }
      if (context.res) {
        try {
          return context.res.writeHead(302, { Location: "/signup" });

          context.res.end();
          context.res.finished = true;
        } catch (error) {}
      }
      return { props: {} }; // will be passed to the page component as props};
    });
  return {
    props: {}, // will be passed to the page component as props
  };
}

/*idToken comes from the client app
admin.auth().verifyIdToken(idToken)
  .then(function(decodedToken) {
    let uid = decodedToken.uid;
    // ...
  }).catch(function(error) {
    // Handle error
  });
//firebase.initializeApp(config);


/*


// idToken comes from the client app
//const idToken = cookies(ctx).myCookie;
console.log('idToken: '+idToken);
admin.auth().verifyIdToken(idToken)
  .then(function(decodedToken) {
    let uid = decodedToken.uid;
    // ...
    console.log('uid: '+uid);
  }).catch(function(error) {
    // Handle error
    console.log('error');
  }); 
  


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://next-283218.firebaseio.com"
});



  //redirect
  import { useRouter } from 'next/router'
/*
  function RedirectPage({ ctx }) {
    const router = useRouter()
    // Make sure we're in the browser
    if (typeof window !== 'undefined') {
      router.push('/new/url');
      return; 
    }
  }
  
  Home.getInitialProps = ctx => {
    // We check for ctx.res to make sure we're on the server.
    if (ctx.res) {
      ctx.res.writeHead(302, { Location: '/new/url' });
      ctx.res.end();
    }
    return { };
  }
*/
export default function Home() {
  return <HomeLayout />;
}
/*
Home.getInitialProps = ctx => {
    const idToken = cookies(ctx).myCookie || '';
    console.log('idToken: '+idToken);
    // Build Firebase credential with the Google ID token.
    var credential = firebase.auth.GoogleAuthProvider.credential(idToken);
    
    // Sign in with credential from the Google user.
    let user=firebase.auth().signInWithCredential(credential);
    console.log('user: '+user);
    if(user.uid){
        let uid = decodedToken.uid;
        // ...
        console.log('uid: '+uid);
    }else{
        if (!ctx.res){console.log('a7a: '+uid);}
        if (ctx.res) {
            ctx.res.writeHead(302, { Location: '/new/url' });
            ctx.res.end();
          }
          return { };
    }
}
    /*
    .then(function(decodedToken) {
        let uid = decodedToken.uid;
        // ...
        console.log('uid: '+uid);
      }).catch(function(error,ctx) {
        if (ctx.res) {
            ctx.res.writeHead(302, { Location: '/new/url' });
            ctx.res.end();
          }
          return { };
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
      console.log('error');
      
    });
    
    }*/
