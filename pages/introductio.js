import dynamic from 'next/dynamic'

const DynamicComponentWithNoSSR2 = dynamic(
  () => import('./introduction'),
  { ssr: false }
)

function Intro() {
  return (
    <div>
      <DynamicComponentWithNoSSR2 />
      <p>HOME PAGE is here!</p>
    </div>
  )
}

export default Intro