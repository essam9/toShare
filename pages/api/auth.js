var admin = require("firebase-admin");
import cookies from "next-cookies";
// idToken comes from the client app

const serviceAccount = require("../../next-283218-firebase-adminsdk-t5k9v-efe01ca1e9.json");
//console.log(serviceAccount);
try {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://next-283218.firebaseio.com",
  });
} catch (err) {
  // we skip the "already exists" message which is
  // not an actual error when we're hot-reloading
  if (!/already exists/.test(err.message)) {
    console.error("Firebase initialization error", err.stack);
    /*build error return outside function
    return {
        props: {}, // will be passed to the page component as props
      }*/
  }
}
const idToken = cookies(context).myCookie || "";
//console.log('idToken: '+idToken);
admin
  .auth()
  .verifyIdToken(idToken)
  .then(function (decodedToken) {
    let uid = decodedToken.uid;
    // ...
    return { idToken: idToken };
    console.log("uid: " + uid);
  })
  .catch(function (error) {
    return { idToken: "error" };
  });
