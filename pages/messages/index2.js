import Link from "next/link";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Button from "@material-ui/core/Button";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCGTFEbmZAg9gnDYSIKbRubz9ymJKTxDuM",
  authDomain: "next-283218.firebaseapp.com",
  databaseURL: "https://next-283218.firebaseio.com",
  projectId: "next-283218",
  storageBucket: "next-283218.appspot.com",
  messagingSenderId: "363937834767",
  appId: "1:363937834767:web:526a25b5a5de2b8069c07a",
  measurementId: "G-RKG4EBC5E4",
};
/*{
  apiKey: 'AIzaSyCGTFEbmZAg9gnDYSIKbRubz9ymJKTxDuM',
  authDomain: 'localhost',
};*/
try {
  firebase.initializeApp(firebaseConfig);
} catch (err) {}
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = user.photoURL;
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    var providerData = user.providerData;

    var db = firebase.firestore();
    
    //
    
  
  }
});  



async function any() {
  var data = [];
  var docRef = firebase.firestore().collection("rooms");
    
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // User is signed in.

      let snapshot = docRef
        .where("users", "array-contains", user.uid)
        .limit(1)
        .get().then(function (snapshot) {
          if (snapshot.empty) {
            console.log("No matching documents.");
            //return {};
          }

          snapshot.forEach((doc) => {
            console.log(doc.id, "=>", doc.data());
            doc.data().id = doc.id;
            data[doc.id] = doc.data(); //working
            data.push(doc.data());
            data[data.length - 1].id = doc.id;
            //return { props: data };

          }); //where room ==

        });
    
    
    } else {
      // No user is signed in.
      console.log('No user is signed in.');
      //return {};
    }
  });
}

any();
    // const res = db.collection("messages").doc("IXRUgpLUaBNkrCa3dowj").set(data); //async
    // Add a new document in collection "cities"
    /*   db.collection("messages")
      .doc()
      .set({
        //worked with doc('LA')
        name: "Los Angeles",
        state: "CA",
        country: "USA",
      })
      .then(function () {
        console.log("Document successfully written!");
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });

    var docRef = db.collection("rooms"); //.doc("SF")
    //var getOptions = { source: "LA" };
    async function xx() {
      let xxx = await docRef.where("from", "==", "").limit(2).get(); //where room ==
      //console.log(xxx.docs[0].data());
      console.log("xxx");
    }
    const list = xx();

    db.collection("rooms")
      .where("from", "==", "")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          // doc.data() is never undefined for query doc snapshots
          console.log(doc.id, " => ", doc.data());
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
    var docRef = db.collection("rooms"); //.where("country", "==", 'USA'); no such doc
    docRef
      .where("from", "==", "GgxooRKMtcXgUbKFFwG7oYGpdea2")
      .limit(2)
      .get()
      .then(function (docs) {
        if (docs[0].exists) {
          console.log("Document data:", docs.data());
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      })
      .catch(function (error) {
        console.log("Error getting document:", error);
      });
  } else {
    // User is signed out.
    // ...
  */
  


const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: "36ch",
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: "inline",
  },
}));

export default function Messages({ props },data) {
  console.log(props);
  console.log(Array.isArray(props));
  console.log(props.length);
  const classes = useStyles();
  const list = data.map((element, index) => (
    <Link href={"/messages/" + element.id}>
      <ListItem alignItems="flex-start" key={"unique"}>
        <ListItemAvatar>
          <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
        </ListItemAvatar>
        <ListItemText
          primary="Brunch this weekend?"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                {element.lastMessage}
              </Typography>
              {" — I'll be in your neighborhood doing errands this…"}
            </React.Fragment>
          }
        />
      </ListItem>
    </Link>
  ));

  if (props.length == 0)
    return <React.Fragment>there is no messages</React.Fragment>;
  else
    return (
      <React.Fragment>
        <Toolbar>
          <Link href="home">
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <ArrowBack />
            </IconButton>
          </Link>
          <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />

          <Typography variant="h6" className={classes.title}>
            Create Activity
          </Typography>
          <Button color="inherit">POST</Button>
        </Toolbar>
        <Divider />

        <List className={classes.root}>
          {list}
          <Divider variant="inset" component="li" />

          <Divider variant="inset" component="li" />
        </List>
      </React.Fragment>
    );
}

Messages.getInitialProps = async (ctx) => {
  // const router = useRouter();
  //const { pid } = router.query;
  //console.log(ctx.query);
  //const userId= firebase.auth().currentUser.uid;
  //console.log('No such document!'+userId)

  /*
  var db = firebase.firestore();
  var data = [];
  var i = 0;
  db.collection("rooms")
    .where("from", "==", "")
    .get()
    .then(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        // doc.data() is never undefined for query doc snapshots
        console.log(doc.id, " => ", doc.data());
        data[i] = doc.data();
        i++;
      });
      console.log(data);
      return { props: data[0] };
    })
    .catch(function (error) {
      console.log("Error getting documents: ", error);
    });
*/
  
  /*
  console.log(snapshot.docs[0].data());
  // const docRef = db.collection("activities").doc(ctx.query.id);

  //const doc = await docRef.get();
  if (!snapshot.docs[0].exists) {
    console.log("No such document!");
    return { props: "No such document!" };
  } else {
    console.log(snapshot.docs[0].data());
    return { props: snapshot.docs[0].data() };
  }
  */
  return {props:[]};
};
/*
 var database = firebase.database();
//create , update
 function writeUserData(userId, name, email, imageUrl) {
    firebase.database().ref('users/' + userId).set({
      username: name,
      email: email,
      profile_picture : imageUrl
    });
  }
//on change
  var starCountRef = firebase.database().ref('posts/' + postId + '/starCount');
starCountRef.on('value', function(snapshot) {
  updateStarCount(postElement, snapshot.val());
});
//read without listening
var userId = firebase.auth().currentUser.uid;
return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
  var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
  // ...
});
*/
