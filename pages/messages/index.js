import Link from "next/link";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Button from "@material-ui/core/Button";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import initialize from "../../firebase-config";

export default class Messages extends React.Component {
  constructor(props) {
        super(props);
        this.state = { value: "", text: "" };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.myRef = {};
        this.classes = {
            footer: {
            border: "solid 1px red",
            },
        };
        console.log("props => " + props);
        console.log(props);
        initialize();
        this.db = firebase.firestore();
        this.userId = firebase.auth().currentUser?.uid;
        console.log(this.userId);
        console.log(firebase.auth().currentUser);
        console.log("pid => " + this.pid);
    }
    
    componentDidMount(ctx) {
        console.log("pidz => " + this.props.pid);
        this.data = [];
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
const query = firebase.firestore().collection('rooms').where("users", "array-contains", user.uid);
            const observer = query.onSnapshot(querySnapshot => {
                console.log(`Received query snapshot of size ${querySnapshot.size}`);
                querySnapshot.forEach((doc) => {

                });
                querySnapshot.docChanges().forEach(change => {
                    if (change.type === 'added') {
                        console.log('New city: ', change.doc.data());
                        var x = document.createElement("P");                        // Create a <p> node
                        var t = document.createTextNode(change.doc.data().lastMessage+"___"+user.uid);
                        var tt = <p>{user.uid}</p>    // Create a text node
                        x.appendChild(t);
                        document.getElementById('container').appendChild(x);
                        //x.appendChild('---');
                        //x.appendChild(tt);  
                        var xx = document.createElement("a");
                        xx.setAttribute("href","/messages/"+change.doc.id);                       // Create a <p> node
                        var tx = document.createTextNode("=>");
                        var tt = <p>{user.uid}</p>    // Create a text node
                        xx.appendChild(tx);                                       // Append the text to <p>
                        document.getElementById('container').appendChild(xx);
                    }
                    if (change.type === 'modified') {
                        console.log('Modified city: ', change.doc.data());
                    }
                    if (change.type === 'removed') {
                        console.log('Removed city: ', change.doc.data());
                    }
                });
            }, err => {
                console.log(`Encountered error: ${err}`);
            });
            } else {
                
            }
            
        });
    }
    handleSubmit() { }
    handleChange(){}
/*
    static async getInitialProps(ctx) {
        initialize();
        var data = [];
        var docRef = firebase.firestore().collection("rooms");
    
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // User is signed in.

      let snapshot = docRef
        .where("users", "array-contains", user.uid)
        .limit(1)
        .get().then(function (snapshot) {
          if (snapshot.empty) {
            console.log("No matching documents.");
            //return {};
          }

          snapshot.forEach((doc) => {
            console.log(doc.id, "=>", doc.data());
            doc.data().id = doc.id;
            data[doc.id] = doc.data(); //working
            data.push(doc.data());
            data[data.length - 1].id = doc.id;
            //return { props: data };

          }); //where room ==

        });
    
    
    } else {
      // No user is signed in.
      console.log('No user is signed in.');
      //return {};
    }
  });




    }
*/
    render() {
        if (this.props.length == 0)
            return <React.Fragment>there is no messages</React.Fragment>;
        else {
            
            
            /*
            const list = this.data.map((element, index) => (
                <Link href={"/messages/" + element.id}>
                    <ListItem alignItems="flex-start" key={"unique"}>
                        <ListItemAvatar>
                            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                        </ListItemAvatar>
                        <ListItemText
                            primary="Brunch this weekend?"
                            secondary={
                                <React.Fragment>
                                    <Typography
                                        component="span"
                                        variant="body2"
                                        className={classes.inline}
                                        color="textPrimary"
                                    >
                                        {element.lastMessage}
                                    </Typography>
                                    {" — I'll be in your neighborhood doing errands this…"}
                                </React.Fragment>
                            }
                        />
                    </ListItem>
                </Link>
            ));

*/
            
            
            
            
            
            
            return (
                <React.Fragment>
                    <Toolbar>
                        <Link href="home">
                            <IconButton
                                edge="start"
                                className={this.classes.menuButton}
                                color="inherit"
                                aria-label="menu"
                            >
                                <ArrowBack />
                            </IconButton>
                        </Link>
                        <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />

                        <Typography variant="h6" className={this.classes.title}>
                            Create Activity
          </Typography>
                        <Button color="inherit">POST</Button>
                    </Toolbar>
                    <Divider />

                    <List className={this.classes.root} id="container">
                        <Divider variant="inset" component="li" />

                        <Divider variant="inset" component="li" />
                    </List>
                </React.Fragment>
            );
        }
}
}
