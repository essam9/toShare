import Link from "next/link";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
//import styles from "./list.module.scss";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import ButtonBase from "@material-ui/core/ButtonBase";
import Button from "@material-ui/core/Button";

import { useRouter } from "next/router";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import styles from "./chats.module.scss";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import initialize from "./../../firebase-config";

export default class Messages extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "", text: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.myRef = {};
    this.classes = {
      footer: {
        border: "solid 1px red",
      },
    };

    console.log("props => " + props);
    console.log(props);
    initialize();
    this.db = firebase.firestore();

    this.userId = firebase.auth().currentUser?.uid;
    console.log(this.userId);
    console.log(firebase.auth().currentUser);
    console.log("pid => " + this.pid);
    /*
    this.snapshot.forEach((doc) => {
      console.log(doc.id, "=>", doc.data());
      doc.data().id = doc.id;
      data[doc.id] = doc.data(); //working
      data.push(doc.data());
      data[data.length - 1].id = doc.id;
    });
    //return { props: data };
    */
  }

  componentDidMount(ctx) {
    //console.log("pidy => " + ctx.query.pid);
    console.log("pidz => "+this.props.pid);
    const query = this.db.collection('messages').where('roomID', '==', this.props.pid);
    const observer = query.onSnapshot(querySnapshot => {
      console.log(`Received query snapshot of size ${querySnapshot.size}`);
      querySnapshot.forEach((doc) => {
       /* var x = document.createElement("P");                        // Create a <p> node
        var t = document.createTextNode("MESSAGE => " + doc.data().message);    // Create a text node
        x.appendChild(t);                                           // Append the text to <p>
        document.getElementById('container').appendChild(x);
        */
      });  
      // ...

querySnapshot.docChanges().forEach(change => {
      if (change.type === 'added') {
        console.log('New city: ', change.doc.data());
        var x = document.createElement("P");                        // Create a <p> node
        var t = document.createTextNode(change.doc.data().message);    // Create a text node
        x.appendChild(t);                                           // Append the text to <p>
        document.getElementById('container').appendChild(x);
      }
      if (change.type === 'modified') {
        console.log('Modified city: ', change.doc.data());
      }
      if (change.type === 'removed') {
        console.log('Removed city: ', change.doc.data());
      }
    });




      //..
    }, err => {
  console.log(`Encountered error: ${err}`);
    });
  }

  static async getInitialProps(ctx) {
    console.log("pidx => "+ctx.query.pid);
    this.pid = ctx.query.pid;
    const pid= ctx.query.pid;
    let data = [];
    //console.log(pid);
    initialize();
    this.data = [];
    this.db = firebase.firestore();
    var docRef = firebase.firestore().collection("messages");
    
    this.snapshot = {};
     let snapshot = await docRef
    .where("roomID", "==", this.pid)
    .limit(1)
    .get(); //where room ==

      if (snapshot.empty) {
        console.log("No matching documents.");
        return;
      }

      snapshot.forEach((doc) => {
        console.log(doc.id, "=>", doc.data());
        doc.data().id = doc.id;
        data[doc.id] = doc.data(); //working
        data.push(doc.data());
        data[data.length - 1].id = doc.id;
      });
      return { data: data,pid:pid };
    
    
    /*
    this.snapshot = await this.db
      .collection("messages")
      //.doc(query.pid + "_" + query.pid)
      //.doc(this.userId + "_" + this.userId)
      //.doc(this.props.stars)
      .where("roomID", "==", ctx.query.pid)
      /*.onSnapshot(
        {
          // Listen for document metadata changes

          includeMetadataChanges: true,
        },
        function (doc) {
          // ...
          console.log(doc.data());
        }
      );
      .limit(1)
      .get() //where room ==
      .then((snapshot) => {
        if (snapshot.empty) {
          console.log("No matching documents");
          return;
        }
        snapshot.forEach((doc) => {
          console.log(doc.id, "=>", doc.data());
          data.push(doc.data());
          data[data.length - 1].id = doc.id;
          console.log("this.data");
          console.log(data);
          return { props: data };
        });
      })
      .catch((err) => {
        console.log(err);
      });
    
    */
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  async handleSubmit(event) {
    event.preventDefault();
    let text = this.state.text;
    this.setState({
      text: text + this.state.value,
    });
    console.log(text + this.state.value);
    await initialize();
    console.log("A name was submitted: " + this.state.value);
    var user = await firebase.auth().currentUser;

    if (user) {
      // User is signed in.
      console.log(user);
    } else {
      // No user is signed in.
      console.log("no user");
    }
    const data = {
      content: this.state.value,
      message: this.state.value,
      userId: firebase.auth().currentUser.uid,
      roomID: this.props.pid,
    };
    var db = firebase.firestore();
    // Add a new document in collection "cities" with ID 'LA'
    const res = db
      .collection("messages")
      .add(data)
      .then(function (docRef) {
        console.log(docRef.id);
        //
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
  }

  render() {
    return (
      <React.Fragment>
        <Toolbar>
          <IconButton
            edge="start"
            className={this.classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
           <Link href="/messages">
                <ArrowBack />
            </Link>
          </IconButton>
          <Avatar></Avatar>
          <Typography variant="h6" className={this.classes.title}>
            Create Activity
          </Typography>
          <Button type="submit" value="Submit" color="inherit">
            POST
          </Button>
        </Toolbar>
        <div className={styles.msgs} id="container">
          <p className={styles.sent}>Hi!</p>
          <p className={styles.recieved}>Hello pro !</p>
          <p className={styles.sent}>Hi!</p>
          <p className={styles.recieved}>Hello pro !</p>
          <p className={styles.sent}>Hi!</p>
          <p className={styles.recieved}>Hello pro !</p>
          <p className={styles.sent}>Hi!</p>
          <p className={styles.recieved}>{this.props.data[0].message}</p>
          <ul>{this.state.text}</ul>
        </div>

        <div className={this.classes.footer}>
          <form
            noValidate
            className={styles.form}
            autoComplete="off"
            onSubmit={this.handleSubmit}
          >
            <input
              id="xx"
              className={styles.input}
              value={this.state.value}
              onChange={this.handleChange}
              label="Outlined"
              variant="outlined"
            />
            <input className={styles.submit} type="submit" value="Submit" />
          </form>
        </div>
      </React.Fragment>
    );
  }
}

/*
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = user.photoURL;
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    var providerData = user.providerData;
    console.log(user.uid);
  } else {
    // User is signed out.
    // ...
  }
});

var db = firebase.firestore();
var userId = firebase.auth().currentUser?.uid;
//console.log(query.pid);
console.log(userId);
db.collection("messages")
  //.doc(query.pid + "_" + query.pid)
  .doc(userId + "_" + userId)
  .onSnapshot(
    {
      // Listen for document metadata changes

      includeMetadataChanges: true,
    },
    function (doc) {
      // ...
    }
  );
//create or update chatRef
//if contains members array equal to [u1,u2],with u1>= u2
//else create page with self created chatId
//with msg saving messages>>chat id >>messageRef
//then add chatRef with members ,title, lastmessage, timestamp=of last message, status

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: "36ch",
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: "inline",
  },
}));

function handleSubmit(event, ref) {
  event.preventDefault();
  console.log(document.getElementById("xx").value);
  const data = {
    from: "Los Angeles",
    to: "CA",
    timestamp: "USA",
  };

  // Add a new document in collection "cities" with ID 'LA'
  const res = db.collection("messages").doc().set(data);
  const res2 = db.collection("rooms").doc().set(data);
}

export default function Messages({ query }) {
  const classes = useStyles();

  var db = firebase.firestore();
  var userId = firebase.auth().currentUser?.uid;
  console.log(query.pid);
  console.log(userId);
  db.collection("messages")
    .doc(query.pid + "_" + query.pid)
    //.doc(userId + "_" + userId)
    .onSnapshot(
      {
        // Listen for document metadata changes

        includeMetadataChanges: true,
      },
      function (doc) {
        // ...
      }
    );
  let myRef;
  
Messages.getInitialProps = ({ query }) => {
  //query.userId = firebase.auth().currentUser.uid;
  return { query };
};
/*
 var database = firebase.database();
//create , update
 function writeUserData(userId, name, email, imageUrl) {
    firebase.database().ref('users/' + userId).set({
      username: name,
      email: email,
      profile_picture : imageUrl
    });
  }
//on change
  var starCountRef = firebase.database().ref('posts/' + postId + '/starCount');
starCountRef.on('value', function(snapshot) {
  updateStarCount(postElement, snapshot.val());
});
//read without listening
var userId = firebase.auth().currentUser.uid;
return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
  var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
  // ...
});
*/
