import Head from "next/head";
import Link from "next/link";
import CssBaseline from "@material-ui/core/CssBaseline";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";

import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import styles from "./profile.module.scss";
import EditIcon from "@material-ui/icons/Edit";
import LocationOn from "@material-ui/icons/LocationOn";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    position: "relative",
    flexDirection: "column",
    minHeight: "100vh",
    padding: "0px",
  },
  large: {
    display: "block",
    margin: "auto",
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
  chip: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(0.5),
    },
  },
  description: {
    display: "flex",
    verticalAlign: "middle",
  },
  p: {
    margin: 0,
    verticalAlign: "middle",
  },
  header: {
    display: "flex",
    position: "relative",
    flexDirection: "column",
  },
  messages: {
    position: "absolute",
    right: theme.spacing(2),
    top: theme.spacing(2),
  },
}));

export default function Profile() {
  const [value, setValue] = React.useState(2);
  const classes = useStyles();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleClick = () => {
    //setValue(newValue);
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" className={classes.root}>
        <AppBar position="relative">
          <Toolbar>
            <Link href="home">
              <IconButton edge="start" color="inherit" aria-label="menu">
                <ArrowBack />
              </IconButton>
            </Link>
          </Toolbar>
        </AppBar>
        <img src="https://picsum.photos/400/150" alt="{tile.title}" />
        <div className={classes.header}>
          <Avatar
            alt="Remy Sharp"
            src="/static/images/avatar/1.jpg"
            className={styles.avatar}
            className={classes.large}
          />
          <h3 className={styles.name}>username</h3>
          <h4 className={styles.handle}>@handle</h4>
          <div className={classes.chip}>
            <Chip
              icon={<EditIcon />}
              label="Edit Profile"
              onClick={handleClick}
            />
          </div>
          <Link
            className={classes.messages}
            color="inherit"
            href="/messages/43OgK1gerwg7ioKEGkwrf3tdEQD2"
          >
            <a className={classes.messages}>messages</a>
          </Link>
        </div>

        <p>Bio Description</p>
        <div className={classes.description}>
          <LocationOn />
          <p className={classes.p}>Description</p>
        </div>
        <p>Website</p>
        <p>Birth Date</p>

        <Paper square>
          <Tabs
            value={value}
            indicatorColor="primary"
            textColor="primary"
            onChange={handleChange}
            aria-label="disabled tabs example"
          >
            <Tab label="Active" />
            <Tab label="Disabled" />
            <Tab label="Active" />
          </Tabs>
        </Paper>
      </Container>
    </React.Fragment>
  );
}
