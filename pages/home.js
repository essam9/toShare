import Head from "next/head";
import Link from "next/link";
import HomeLayout from "../Components/global/HomeLayout";
import React, { useState, useEffect } from 'react';
//-----------redirect
/*
export async function getServerSideProps(context) {
return context.res.writeHead(302, { Location: '/signup' });
*/

import initialize from "../firebase-config";
import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

initialize();


export default function Home() {

  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  var data = [];
//const classes = useStyles();
useEffect(() => {
    // Update the document title using the browser API
 //document.title = `You clicked times`;
  firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = user.photoURL;
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    var providerData = user.providerData;
    console.log(user.uid);
    //window.location = "http://localhost:3000/home2";
    //window.location = "home2";
    firebase.firestore().collection("activities").limit(20)//where("capital", "==", true)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
          /*  var x = document.createElement("P");                        // Create a <p> node
            var t = document.createTextNode(doc.data().content);    // Create a text node
          x.appendChild(t);                                        // Append the text to <p>
          var y = document.createElement("div");
          y.innerHTML = "<p>fghhgf</p>";
            document.getElementById('container').appendChild(x);
          document.getElementById('container').appendChild(y);
*/
          setIsLoaded(true);
          data[doc.id]=doc.data();
          setItems(data);
        });

      console.log(items);

      
    })
      .catch(function (error) {
      setIsLoaded(true);
          setError(error);
        console.log("Error getting documents: ", error);
      });
  } else {
    // User is signed out.
    // ...
    window.location = "/signin";
  }
});
  },[]);

  return <HomeLayout />;
  
}
