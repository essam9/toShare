import Head from "next/head";
import Link from "next/link";
import HomeLayout from "../../Components/global/HomeLayout";

export default function Home() {
  return <HomeLayout />;
}
