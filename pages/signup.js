// Import FirebaseAuth and firebase.
import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "firebase";
import { useRouter } from "next/router";

import initialize from "./../firebase-config";
class SignInScreen extends React.Component {
  // The component's Local state.
  state = {
    isSignedIn: false, // Local signed-in state.
  };

  // Configure FirebaseUI.
  uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: "popup",
    signInSuccessURL: "/home",
    // We will display Google and Facebook as auth providers.
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      //firebase.auth.AppleAuthProvider.PROVIDER_ID,
      firebase.auth.PhoneAuthProvider.PROVIDER_ID,
    ],
    callbacks: {
      // Avoid redirects after sign-in.
      //signInSuccessWithAuthResult: () => false,
      //set cookie
      //redirect
      //check if users.uid or create
      //usersFunction();
      


    },
  };
  async usersFunction() {
    var db = firebase.firestore();
    const userRef = db.collection('users').where('uid', '==', user.uid).get();
    if (!userRef.exists) {
      console.log('No such document!');
      var data={uid:user.uid}
      const res = db
      .collection("users")
      .add(data)
      .then(function (docRef) {
        console.log(docRef.id);
        //router.push({
        //  pathname: "/events/" + docRef.id,
          //query: {q: query},
        //});
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
    } else {
      console.log('Document data:', doc.data());
    }
    // Add a new document in collection "cities" with ID 'LA'
    
}
  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    initialize();
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
         //check if users.uid or create
        var db = firebase.firestore();
        const docRef = db.collection('users').where('uid', '==', user.uid).get().then(function (querySnapshot) {
            if (querySnapshot.size > 0) {
              querySnapshot.forEach(function(doc) {
                // doc.data() is never undefined for query doc snapshots
                console.log(doc.id, " => ", doc.data());
              })
            } else {
              console.log('No such document!');
              var data = {
                uid: user.uid,
                email: user.email,
                displayName: user.displayName,
                emailVerified: user.emailVerified,
                providerData : user.providerData,
                photoURL : user.photoURL,
              }
              const res = db
                .collection("users")
                .add(data)
                .then(function (docRef) {
                  console.log(docRef.id);
                  //router.push({
                  //  pathname: "/events/" + docRef.id,
                  //query: {q: query},
                  //});
                })
                .catch(function (error) {
                  console.error("Error writing document: ", error);//this giving no permission must be handeled
                });  
              }
            }).catch(function(error) {
              console.log("Error getting document:", error);
          });
        
 

        this.setState({ isSignedIn: !!user });
        firebase
          .auth()
          .currentUser.getIdToken(/* forceRefresh */ true)
          .then(function (idToken) {
            // Send token to your backend via HTTPS
            // ...
            console.log("did" + idToken + "____" + user);
            console.log(user);
          })
          .catch(function (error) {
            // Handle error
          });
        window.location.replace("/home");

      }
    });
  }
  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  render() {
    initialize();
    return (
      <div>
        <h1>My App</h1>
        <p>Please sign-in:</p>
        <StyledFirebaseAuth
          uiConfig={this.uiConfig}
          firebaseAuth={firebase.auth()}
        />
      </div>
    );
  }
}
export default SignInScreen;
