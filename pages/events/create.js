import Link from "next/link";
import React from "react";
import { useState } from "react";
import { useRouter } from "next/router";

import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
//import Typography from '@material-ui/core/Typography';
//AppBar
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import ImageOutlined from "@material-ui/icons/ImageOutlined";
import VideocamOutlined from "@material-ui/icons/VideocamOutlined";

//textfield
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
//chat
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
//import Create from '../../Components/Events/Create';
import styles from "./create.module.scss";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCGTFEbmZAg9gnDYSIKbRubz9ymJKTxDuM",
  authDomain: "next-283218.firebaseapp.com",
  databaseURL: "https://next-283218.firebaseio.com",
  projectId: "next-283218",
  storageBucket: "next-283218.appspot.com",
  messagingSenderId: "363937834767",
  appId: "1:363937834767:web:526a25b5a5de2b8069c07a",
  measurementId: "G-RKG4EBC5E4",
};
/*{
  apiKey: 'AIzaSyCGTFEbmZAg9gnDYSIKbRubz9ymJKTxDuM',
  authDomain: 'localhost',
};*/
try {
  firebase.initializeApp(firebaseConfig);
} catch (err) {}
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = user.photoURL;
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    var providerData = user.providerData;
    console.log(user.uid);
  } else {
    // User is signed out.
    // ...
  }
});

//create or update chatRef
//if contains members array equal to [u1,u2],with u1>= u2
//else create page with self created chatId
//with msg saving messages>>chat id >>messageRef
//then add chatRef with members ,title, lastmessage, timestamp=of last message, status

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    minHeight: "80vh",
    maxWidth: "36ch",
    backgroundColor: theme.palette.background.paper,
  },

  inline: {
    display: "inline",
  },
  root2: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  textfield: {
    flexGrow: 1,
    position: "absolute",
    bottom: "10px",
    display: "block",
    margin: "0 auto",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(0),
  },
}));
function handleChange() {
  console.log("handleChange");
}
function handleSelectChange(event) {
  this.setState({ select: event.target.value });
  console.log(event.target.value);
  console.log(this.select);
}
function handleSubmit(event) {
  event.preventDefault();
  console.log(event);
}
const preventDefault = (f) => (e) => {
  e.preventDefault();
  f(e);
};
export default function Messages() {
  const [input, setInput] = useState(""); // '' is the initial state value

  const classes = useStyles();
  const createMessage = () => (event) => {};

  const person = "";
  /////////////////////////
  const router = useRouter();
  const [query, setQuery] = useState("");
  const [privacy, setPrivacy] = useState("");
  const handleParam = (setValue) => (e) => {
    setValue(e.target.value);
    console.log(e.target.value);
  };
  const handleSubmit = preventDefault(() => {
    console.log(query);
    /*router.push({
    pathname: action,
    query: {q: query},
  })*/

    const data = {
      content: query,
      userId: firebase.auth().currentUser.uid,
      timestamp: new Date(),
      location: "",
      media: "",
      preferredGender: "",
      privacy: privacy,
      rangeLocation: "",
      rangeTime: "",
      title: "",
    };
    var db = firebase.firestore();
    // Add a new document in collection "cities" with ID 'LA'
    const res = db
      .collection("activities")
      .add(data)
      .then(function (docRef) {
        console.log(docRef.id);
        router.push({
          pathname: "/events/" + docRef.id,
          //query: {q: query},
        });
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
  });
  //////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <Link href="/home">
              <a>
                <ArrowBack />
              </a>
            </Link>
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Create Activity
          </Typography>
          <Button type="submit" value="Submit" color="inherit">
            POST
          </Button>
        </Toolbar>
        <Divider />
        <Grid container spacing={1}>
          <Grid item xs={2} alignItems="center">
            <Avatar className={styles.avatar} />
          </Grid>
          <Grid item xs={10}>
            <Typography
              className={styles.name}
              style={{ "margin-top": "0.7rem", "font-size": "large" }}
              gutterBottom
            >
              Essam Ewais <span className={styles.handle}> @essam </span>
            </Typography>

            <select value={privacy} onChange={handleParam(setPrivacy)}>
              <option value="m1">everyone</option>
              <option value="m2">c</option>
              <option value="m3">c</option>
              <option value="m4">c</option>
            </select>
          </Grid>
          <Grid item xs={12}>
            <Typography>
              <textarea
                id="txt"
                className={styles.text}
                value={query}
                onChange={handleParam(setQuery)}
                rows="6"
              ></textarea>
            </Typography>
            <textarea
              id="txt"
              className={styles.text}
              value={query}
              onChange={handleParam(setQuery)}
              rows="6"
            ></textarea>
          </Grid>
          <Divider />
          <Grid item xs={12}>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <ImageOutlined />
            </IconButton>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <VideocamOutlined />
            </IconButton>
          </Grid>
        </Grid>
      </form>
    </React.Fragment>
  );
}
