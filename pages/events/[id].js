import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";

import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import Chip from "@material-ui/core/Chip";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCGTFEbmZAg9gnDYSIKbRubz9ymJKTxDuM",
  authDomain: "next-283218.firebaseapp.com",
  databaseURL: "https://next-283218.firebaseio.com",
  projectId: "next-283218",
  storageBucket: "next-283218.appspot.com",
  messagingSenderId: "363937834767",
  appId: "1:363937834767:web:526a25b5a5de2b8069c07a",
  measurementId: "G-RKG4EBC5E4",
};
try {
  firebase.initializeApp(firebaseConfig);
} catch (err) {}
var db = firebase.firestore();

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

function Event({ stars }) {
  const router = useRouter();
  const { id } = router.query;
  console.log(id);
  console.log(stars);
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const handleDelete = () => {
    console.info("You clicked the delete icon.");
  };

  const handleClick = () => {
    console.info("You clicked the Chip.");
  };
  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
            R
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={stars.userId}
        subheader="September 14, 2016"
      />
      <CardMedia
        className={classes.media}
        image="/static/images/cards/paella.jpg"
        title="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {stars.content}
          <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          <Chip
            label="Clickable deletable"
            onClick={handleClick}
            onDelete={handleDelete}
          />
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
}
Event.getInitialProps = async (ctx) => {
  // const router = useRouter();
  //const { pid } = router.query;
  console.log(ctx.query);
  //const userId= firebase.auth().currentUser.uid;
  //console.log('No such document!'+userId)
  const docRef = db.collection("activities").doc(ctx.query.id);

  const doc = await docRef.get();
  if (!doc.exists) {
    console.log("No such document!");
    return { stars: "No such document!" };
  } else {
    return { stars: doc.data() };
  }
  /*
docRef.get().then(function(doc) {
    if (doc.exists) {
        console.log("Document data:", doc.data());
        return doc.data();
    } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
    }
}).catch(function(error) {
    console.log("Error getting document:", error);
});

  const res = await fetch('https://api.github.com/repos/vercel/next.js')
  const json = await res.json()
  */
  return { stars: "k" };
};
export default Event ;
 