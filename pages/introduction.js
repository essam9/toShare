// Import FirebaseAuth and firebase.
import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "firebase";

// Configure Firebase.
const firebaseConfig = {
  apiKey: "AIzaSyCGTFEbmZAg9gnDYSIKbRubz9ymJKTxDuM",
  authDomain: "next-283218.firebaseapp.com",
  databaseURL: "https://next-283218.firebaseio.com",
  projectId: "next-283218",
  storageBucket: "next-283218.appspot.com",
  messagingSenderId: "363937834767",
  appId: "1:363937834767:web:526a25b5a5de2b8069c07a",
  measurementId: "G-RKG4EBC5E4",
};
try {
  //let firebase = {};
  firebase.initializeApp(firebaseConfig);
  console.log("initialize");
} catch (err) {
  console.log(err);
}

class SignInScreen extends React.Component {
  // The component's Local state.
  state = {
    isSignedIn: false, // Local signed-in state.
  };

  // Configure FirebaseUI.
  uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: "popup",
    // We will display Google and Facebook as auth providers.
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    ],
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccessWithAuthResult: () => false,
      //set cookie
      //redirect
    },
  };

  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged((user) => {
      this.setState({ isSignedIn: !!user });
      firebase
        .auth()
        .currentUser.getIdToken(/* forceRefresh */ true)
        .then(function (idToken) {
          // Send token to your backend via HTTPS
          // ...
        })
        .catch(function (error) {
          // Handle error
        });
      myfunc();
    });
  }
  async myfunc() {
    let result = await fetch("");
  }
  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  render() {
    if (!this.state.isSignedIn) {
      return (
        <div>
          <div style={{ textAlign: "center" }}>
            <img width="50%" height="auto" src="/images/popcorn.png" />
            <h1>toShare</h1>
          </div>
          <h1>toShare</h1>
          <p>Interact with people in real life.</p>
          <StyledFirebaseAuth
            uiConfig={this.uiConfig}
            firebaseAuth={firebase.auth()}
          />
        </div>
      );
    }
    return (
      <div>
        <h1>My App</h1>
        <p>
          Welcome {firebase.auth().currentUser.displayName}! You are now
          signed-in!
        </p>
        <a onClick={() => firebase.auth().signOut()}>Sign-out</a>
      </div>
    );
  }
}
export default SignInScreen;
